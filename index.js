const mongoose = require("mongoose");
const cors = require("cors");
const express = require("express");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z2lte8d.mongodb.net/b203_bookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to the cloud database"));

const port = process.env.PORT || 4000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));

