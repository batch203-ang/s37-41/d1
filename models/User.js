/*
	user {

		id - unique identifier for the document,
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
		enrollments: [
			{

				id - document identifier,
				courseId - the unique identifier for the course,
				courseName - optional,
				isPaid,
				dateEnrolled
			}
		]

	}
*/

const mongoose = require("mongoose");

let userSchema = mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type:String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn :{
		type: Date,
		default: new Date()
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required"]
			},
			courseName: {
				type: String,
				required: [true, "Course Name is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			isPaid: {
				type: Boolean,
				default:true
			}
		}
	]
});

module.exports = mongoose.model("users", userSchema);