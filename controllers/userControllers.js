const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
/*
	Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) => {
		User.find({email: req.body.email})
		.then(result => {
			console.log(result);
			if(result.length > 0){
				//return res.send(true)
				res.send("User already exists")
			}
			else{
				//return res.send(false)
				res.send("Email available")
			}
		})
		.catch(error =>  res.send(error));
};

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {
	
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	});
	console.log(newUser);
	return newUser.save()
	.then(user => {
		console.log(newUser);
		res.send(true);
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			//return res.send(false)
			return res.send({message: "No user found"})
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return res.send({message: "Incorrect password!"});
			}
		}
	});
};

// S38 Activity
/*
    1. Create a "/details/:id" route that will accept the user’s Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user:
        - Find the document in the database using the user's ID
        - Reassign the password of the returned document to an empty string ("") / ("*****")
        - Return the result back to the postman
    3. Process a GET request at the /details/:id route using postman to retrieve the details of the user.
    4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
    5. Add the link in Boodle.

*/
/*module.exports.getDetails = (req, res) =>{
	User.findById(req.params.id)
	.then(user => {
		//let censoredPW = {
			//_id: req.params.id,
			//firstName: user.firstName,
			//lastName: user.lastName,
			//email: user.email,
			//user.password: "",
			//mobileNumber: user.mobileNumber,
			//isAdmin: user.isAdmin,
			//createdOn: user.createdOn,
			//enrollments: user.enrollments
		//};
		//res.send(censoredPW);

		user.password = "";
		return res.send(user);
		})
	.catch(error => res.send("User not found"));
};*/

module.exports.getDetails = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);
	console.log();
	console.log(userData);

	return User.findById(userData.id)
	.then(user => {
		user.password = "";
		return res.send(user);
		})
	.catch(error => res.send("User not found"));
};

// Enroll a Course
/*
	Steps:
	// Users
	1. Find the document in the database using the user's ID (token)
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
	//Courses
	1. Find the document in the database using the course ID provided in the request body.
	2. Add the user ID to the course enrollees array.
	3. Update the document in the MongoDB Atlas Database
*/

// Async await will be used in enrolling the user because we will need to update 2 separate douments when enrolling a user.
module.exports.enroll = async(req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let courseName = await Course.findById(req.body.courseId).then(result => result.name);

	let data = {
		userId: userData.id,
		email: userData.email,
		courseId: req.body.courseId,
		courseName: courseName
	}

	console.log(data);

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({
			courseId: data.courseId,
			courseName: data.courseName
		});
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		});
	});

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({
			userId: data.userId,
			email: data.email
		});
		course.slots -= 1;
		
		return course.save()
		.then(result => {
			console.log(result);
			return true
		})
		.catch(error => {
			console.log(error);
			return false;
		});
	});
	console.log(isCourseUpdated);

	(isUserUpdated && isCourseUpdated) ? res.send(true) : res.send(false);
};