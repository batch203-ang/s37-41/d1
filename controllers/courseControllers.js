const Course = require("../models/Course");
const auth = require("../auth");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (req, res) => {
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
	});
	//console.log(newCourse);

	let user = auth.decode(req.headers.authorization);
	if(user.id != undefined && user.email != undefined && user.isAdmin != undefined){

		if(user.isAdmin == true){
			newCourse.save()
			.then(course => {
				console.log(newCourse);
				res.send(true);
			})
			.catch(error => {
				console.log(error);
				res.send(false);
			})
		}else{
			res.send("User is not authorized to register a course.");
		}
	}else{
		res.send("No token provided");
	}
};

module.exports.getAllCourses = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		Course.find({})
		.then(result => res.send(result));
		//.catch(error => res.send(error));
	}
	else{
		return res.status(401).send("You dont have access to this page!");
	}
};

module.exports.getAllActive = (req, res) => {
	return Course.find({isActive: true})
	.then(result => res.send(result));
};

module.exports.getCourse = (req, res) => {
	console.log(req.params.courseId);
	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body

*/
module.exports.updateCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		};

		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new: true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		});
	}
	else{
		return res.status(401).send("You dont have access to this page!");
	}
};

module.exports.archiveCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let updateIsActiveField = {
			isActive: req.body.isActive
		};

		return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField, {new: true})
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else{
		return res.status(401).send("You dont have access to this page!");
	}
};