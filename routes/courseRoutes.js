const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

console.log(courseController);

router.post("/", auth.verify, courseController.addCourse);
router.get("/all", auth.verify, courseController.getAllCourses);
router.get("/", courseController.getAllActive);
router.get("/:courseId", courseController.getCourse);
router.put("/:courseId", courseController.updateCourse);
router.patch("/archive/:courseId", courseController.archiveCourse);

module.exports = router;