const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);
router.post("/checkEmail", userControllers.checkEmailExists);
router.post("/register", userControllers.registerUser);
router.post("/login", userControllers.loginUser);
router.get("/details", auth.verify, userControllers.getDetails);
router.post("/enroll", auth.verify, userControllers.enroll);

module.exports = router;